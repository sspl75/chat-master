# Chat Master  

###Einleitung  

Chat Master ist eine Website, welche sehr erweiterbar ist. Die Idee kam mir, als meine Mutter   
anfing mehr und mehr im Internet zu machen und oft nicht wusste, was manche Dinge heißen.  
Mein Ziel war also eine Seite zu erstellen, welche es vereinfacht sich in Chats wie Discord oder Ähnlichem zurechtzufinden.  
Dafür benutzt die Seite primär die "Urban-Dictionary" API, welche es möglich macht abkürzungen,  
die meist aus dem Englischen stammen, und andere Wörter vereinfacht zu erklären.  
Dazu Habe ich noch zwei andere, eher gimmickhafte APIs eingebaut, welche beispielsweise lustige Gesichter   
oder einen stilisierten Text zur Verfügung stellen.  

###APIs
1. Urban Dictionary API: https://rapidapi.com/community/api/urban-dictionary  
2. ASCII Text API: http://artii.herokuapp.com/
3. Lenny API: https://github.com/LennyToday/RESTful-lenny  

###Installation  

**1. In den geplanten Zielordner navigieren**  

    cd zielordner  
    
**2. Repository Klonen**  

    git clone https://git.thm.de/sspl75/chat-master.git
    
**3. Dev-Dependencies installieren**

    npm install
    
**4. Projekt Starten**

    npm start
    
**5. Projekt aufrufen**

Das Projekt befindet sich auf localhost:3000.