const request = require(`request`);
const express = require('express');
const app = express();
app.set(`port`,process.env.PORT || 3000)

//Middleware Call
app.use(express.static("./express"));

//Proxy Call for API
app.get('/ascii', (req, res) => {
    request(
        { url: req.query.address },
        (error, response, body) => {
            if (error || response.statusCode !== 200) {
                return res.status(500).json({ type: 'error', message: `ERROR` });
            }
            res.send(body);
        }
    )
});

//Error Handling Client
app.use(function (req,res){
    res.type("text/plain");
    res.status(404);
    res.send("404 - Not Found");
});

//Error Handling Server
app.use(function (err, req, res, next){
    console.error(err.stack);
    res.type("text/plain");
    res.status(500);
    res.send("500 - Internal Server Error");
});

exports.start = function (){
    app.listen(app.get(`port`),function (){
        console.log("Express ready on http://127.0.0.1:" + app.get(`port`));
    });
};
